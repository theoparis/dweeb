export type TypedArray =
  | Float32Array
  | Float64Array
  | Int8Array
  | Int16Array
  | Int32Array
  | Uint8ClampedArray
  | Uint8Array
  | Uint16Array
  | Uint32Array;
export type TypedArrayConstructor = { new (data: number[]): TypedArray };
export type ITensor<S extends number[]> = {
  shape: S;
  data: number[];
};
export type TensorId<S extends number[]> = `${S[0]}x${S[1]}`;
export type IMat4 = ITensor<[4, 4]>;
export type IVec3 = ITensor<[3]>;

export class Tensor<S extends number[]> {
  constructor(public shape: S, public data: number[]) {
    // Check if the shape is valid against the data length
    if (shape.reduce((s, l) => s * l, 1) !== data.length)
      throw new Error(`Invalid shape ${shape} and data length ${data.length}`);
  }

  static create<S extends number[]>(shape: S, data?: number[]): Tensor<S> {
    return new Tensor(shape, data || new Array(shape[0] * shape[1]));
  }

  get x() {
    return this.data[0];
  }

  get y() {
    return this.data[1];
  }

  get z() {
    return this.data[2];
  }

  get w() {
    return this.data[3];
  }

  static translation(vec: Tensor<[3]>) {
    // translation matrix
    const m = Tensor.create<[4, 4]>(
      [4, 4],
      [1, 0, 0, vec.x, 0, 1, 0, vec.y, 0, 0, 1, vec.z, 0, 0, 0, 1]
    );

    return m;
  }

  static rotation(vec: Tensor<[3]>) {
    // rotation matrix
    const m = Tensor.create<[4, 4]>(
      [4, 4],
      [1, 0, 0, vec.x, 0, 1, 0, vec.y, 0, 0, 1, vec.z, 0, 0, 0, 1]
    );

    return m;
  }

  static scale(vec: Tensor<[3]>) {
    // scale matrix
    const m = Tensor.create<[4, 4]>(
      [4, 4],
      [vec.x, 0, 0, 0, 0, vec.y, 0, 0, 0, 0, vec.z, 0, 0, 0, 0, 1]
    );

    return m;
  }

  multiply(m: Tensor<[4, 4]>) {
    // matrix multiplication
    const a = this.data;
    const b = m.data;
    const c = new Array(a.length);

    for (let i = 0; i < a.length; i += 4) {
      for (let j = 0; j < 4; j++) {
        let sum = 0;
        for (let k = 0; k < 4; k++) {
          sum += a[i + k] * b[k * 4 + j];
        }
        c[i + j] = sum;
      }
    }

    return new Tensor<[4, 4]>([4, 4], c);
  }

  static perspective(fov: number, aspect: number, near: number, far: number) {
    const f = 1.0 / Math.tan(fov * 0.5);

    return new Tensor<[4, 4]>(
      [4, 4],
      [
        f / aspect,
        0,
        0,
        0,
        0,
        f,
        0,
        0,
        0,
        0,
        (far + near) / (near - far),
        -1,
        0,
        0,
        (2 * far * near) / (near - far),
        0,
      ]
    );
  }

  asTypedArray<D extends TypedArray = Float32Array>(type: D) {
    return new (type.constructor as TypedArrayConstructor)(this.data);
  }

  static zeros<S extends number[]>(shape: S): Tensor<S> {
    const data = new Array<number>(shape.reduce((s, l) => s * l, 1)).fill(0);

    return new Tensor(shape, data);
  }

  static random<S extends number[]>(shape: S, multiplier = 1): Tensor<S> {
    const tensor = Tensor.zeros(shape);
    tensor.data = tensor.data.map((_, _i) => Math.random() * multiplier);
    return tensor;
  }

  static identity<S extends number[]>(shape: S): Tensor<S> {
    // identity matrix
    const size = shape.reduce((s, l) => s * l, 1);
    const data = new Array(size).fill(0);

    for (let i = 0; i < size; i += shape[0] + 1) {
      data[i] = 1;
    }

    return new Tensor(shape, data);
  }
}

export function isOfSize<S extends number[]>(
  tensor: Tensor<number[]>,
  size: S
): tensor is Tensor<S> {
  return (
    size.length === tensor.shape.length &&
    size.every((d, i) => tensor.shape[i] === d)
  );
}
