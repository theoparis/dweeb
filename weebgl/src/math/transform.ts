import { Component } from "../ecs/ecs.ts";
import { Tensor } from "./tensor.ts";

export class Transform extends Component {
  constructor(
    public position: Tensor<[3]> = Tensor.zeros([3]),
    public rotation: Tensor<[3]> = Tensor.zeros([3]),
    public scale: Tensor<[3]> = Tensor.create([3], [1, 1, 1])
  ) {
    super();
  }

  /**
   * Returns a transform matrix using the transform's position, rotation and scale.
   */
  get matrix(): Tensor<[4, 4]> {
    // TODO: implement
    return Tensor.translation(this.position)
      .multiply(Tensor.rotation(this.rotation))
      .multiply(Tensor.scale(this.scale));
  }
}
