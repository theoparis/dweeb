import { Component } from "../ecs/ecs.ts";
import { createDefaultMaterial, Material } from "../renderEngine/material.ts";
import { IModel } from "../renderEngine/model.ts";

/**
 * A component that defines properties that can be used to render a model.
 */
export class Renderable extends Component {
  constructor(
    public model: IModel,
    public material: Material = createDefaultMaterial()
  ) {
    super();
  }
}
