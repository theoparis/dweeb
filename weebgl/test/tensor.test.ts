import { assertEquals } from "https://deno.land/std/testing/asserts.ts";
import { Tensor } from "../src/math/tensor.ts";

Deno.test("tensor identity", async () => {
  const t1 = Tensor.identity([4, 4]);

  assertEquals(t1.data, [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]);
});

Deno.test("perspective", async () => {
  const t1 = Tensor.perspective(60, 1, 0.1, 100);

  assertEquals(
    t1.data,
    [
      -0.15611995216165922, 0, 0, 0, 0, -0.15611995216165922, 0, 0, 0, 0,
      -1.002002002002002, -1, 0, 0, -0.20020020020020018, 0,
    ]
  );
});
