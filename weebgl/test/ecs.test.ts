import { assertEquals } from "https://deno.land/std/testing/asserts.ts";
import { ECS } from "../src/ecs/ecs.ts";
import { Tensor } from "../src/math/tensor.ts";
import { Transform } from "../src/math/transform.ts";

Deno.test("add entity", async () => {
  const ecs = new ECS();
  const entity = ecs.addEntity();

  assertEquals(entity, 0);
});

Deno.test("add entity with components", async () => {
  const ecs = new ECS();
  const entity = ecs.addEntity();
  const position = new Transform(Tensor.create<[3]>([3], [1, 2, 3]));
  ecs.addComponent(entity, position);
  const container = ecs.getComponents(entity);

  assertEquals(container?.has(Transform), true);
  assertEquals(container?.get(Transform)?.position.data, [1, 2, 3]);
});
