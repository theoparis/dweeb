import { sprintf, printf } from "https://deno.land/std/fmt/printf.ts";
import { red, yellow, gray, cyan } from "https://deno.land/std/fmt/colors.ts";

export type LogLevel = "info" | "warn" | "error" | "debug";

export interface LoggerOptions {
  level: LogLevel;
  format?: string;
}

export type ILogger = {
  [L in LogLevel]: (...messages: unknown[]) => void;
};

const initialOptions = { level: "error", format: "%s\n" };

export class Logger {
  private _level: LogLevel;
  private _format: string;

  constructor(options: LoggerOptions = initialOptions as LoggerOptions) {
    const { level, format } = { ...initialOptions, ...options };
    this._level = level;
    this._format = format;
  }

  get level(): LogLevel {
    return this._level;
  }

  set level(_l: LogLevel) {
    this._level = _l;
  }

  get format(): string {
    return this._format;
  }

  set format(_f: string) {
    this._format = _f;
  }

  log(...messages: unknown[]) {
    printf(gray(sprintf(this.format, this.level, ...messages)));
  }

  info(...messages: unknown[]) {
    printf(cyan(sprintf(this.format, ...messages)));
  }

  warn(...messages: unknown[]) {
    printf(yellow(sprintf(this.format, ...messages)));
  }

  error(...messages: unknown[]) {
    printf(red(sprintf(this.format, ...messages)));
  }
}
